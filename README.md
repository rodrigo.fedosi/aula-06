## Introduction

Trabalho aula06, pipe-line Gitlab. Membros do grupo: Rodrigo, Cintia e Rubens.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [.NET Hello World tutorial](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/)

If you're new to .NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## O que contem neste projeto

```
stages:
    - build
    - test
```